# Network Message

This library is designed to ease network traffic.  
By using specific topics and handlers it is easy to separate functionality between different payloads.  
To reduce network load all handlers operate in Buffers.  

## Content

### [Message](doc/Message.md)
### [MessageHandler](doc/MessageHandler.md)
### [Channel](doc/Channel.md)
### [ChannelHandler](doc/ChannelHandler.md)

## Example

An example implementation could look like this.  

Process A:

```typescript
const readHandler = (buffer: ArrayBuffer) => {
    const decoder = new TextDecoder();
    const header = new Uint32Array(buffer.slice(0, 4));
    const length = header.at(0);
    const data = decoder.decode(buffer.slice(4, 4 + length!));

    return data;
};

const writeHandler = (data: string) => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode(data);
    const length = encoded.byteLength;

    return new Uint8Array(Array.of(
        ...new Uint8Array(new Uint32Array(Array.of(length)).buffer),
        ...encoded
    )).buffer;
};

const messageHandler = new MessageHandler();
messageHandler.on(0, readHandler, writeHandler);

const buffer = messageHandler.write(new Message(0, "example"));
// send buffer over network to Process B
```

Process B:

```typescript
const readHandler = (buffer: ArrayBuffer) => {
    const decoder = new TextDecoder();
    const header = new Uint32Array(buffer.slice(0, 4));
    const length = header.at(0);
    const data = decoder.decode(buffer.slice(4, 4 + length!));

    return data;
};

const writeHandler = (data: string) => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode(data);
    const length = encoded.byteLength;

    return new Uint8Array(Array.of(
        ...new Uint8Array(new Uint32Array(Array.of(length)).buffer),
        ...encoded
    )).buffer;
};

const messageHandler = new MessageHandler();
messageHandler.on(0, readHandler, writeHandler);

const channel = new Channel();
channel.on(0, function(socket, data: string) {
    console.log(data);
    // would log 'example'
});

const channelHandler = new ChannelHandler();
channelhandler.setChannel(0, channel);

// receive buffer from Process A
const buffer = new ArrayBuffer(); // only example, normally filled
const messages = messageHandler.read(buffer);

channelHandler.receive(messages);
```

## License

Copyright 2025 Benedikt Warmesbach

Licensed under the Apache license, version 2.0 (the "license"); You may not use this file except in compliance with the license. You may obtain a copy of the license at:

http://www.apache.org/licenses/LICENSE-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the license is distributed on an "as is" basis, without warranties or conditions of any kind, either express or implied. See the license for the specific language governing permissions and limitations under the license.