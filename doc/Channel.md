# Channel

A channel is used to separate out message traffic and make type conversion easier.  
To use a channel you will typically need a [ChannelHandler](./ChannelHandler.md).  

A channel can be set up with different callbacks depending on the topic you want to listen to:

```typescript
const channel = new Channel();
channel.on(0, function(socket, data: boolean) {
    // implementation
});

channel.on(1, function(socket, data: string) {
    // another implementation
});
```

It is possible to manually trigger a channel:

```typescript
const message = new Message(0, "test");
channel.receiveMessage(message);
```

A channel will check the topic of the message it receives and trigger the corresponding callback.