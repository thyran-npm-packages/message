# ChannelHandler

A ChannelHandler creates the opportunity to separate message traffic over different channels enabling a logical separation of implementations.  
It also provides a function to handle an iterator over messages e.g. from [MessageHandler](./MessageHandler.md)'s read function.  

Keep in mind that in order to have a channel receive data from the handler it has to be bound to that topic:

```typescript
const channel = new Channel();
channel.on(0, function(socket, data) { /* ... */ });
channel.on(1, function(socket, data) { /* ... */ });

const channelHandler = new ChannelHandler();
channelHandler.setChannel(0, channel);
channelHandler.setChannel(1, channel);
```

If you do not bind a channel to a topic in the ChannelHandler, that Channel will not execute its callback for the topic.