# Message

An object that holds a topic and data.  
Typically you would not instanciate this yourself, but rather use a [MessageHandler](./MessageHandler.md).  

If you need have no need to use a full handler, you can always use the Message object itself:

```typescript
type TestData = { test: string }
const testData: TestData = { test: "test" };

const encoder = new TextEncoder();

const writeHandler = (data: TestData) => {
    const body = encoder.encode(data.test).buffer;
    const header = new Uint32Array(Array.of(body.byteLength)).buffer;

    return new Uint8Array(Array.of(
        ...new Uint8Array(header),
        ...new Uint8Array(body)
    )).buffer;
}

const message = new Message(0, testData);
const buffer = message.toBuffer(writeHandler);

// and on the other side:
type TestData = { test: string };
const buffer = new ArrayBuffer(); // the arraybuffer from the previous message

const readHandler = (buffer: ArrayBuffer) => {
    const header = new Uint32Array(buffer.slice(0, 4));
    const length = header.at(0)!;
    const data = decoder.decode(buffer.slice(4, 4 + length));

    return { test: data } as TestData;
};

const messages = Message.fromBuffer(buffer);
const message = Array.from(messages).at(0)?.toMessage(readHandler);

// note that `messages` is a generator as it can happen that messages concatenate in the network stream
// also note that `message` can be undefined because it is possible to read a buffer that does not produce a Message
```