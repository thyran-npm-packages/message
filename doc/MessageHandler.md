# MessageHandler

Use a MessageHandler to read or write messages using predefined functions.  
The MessageHandler stores read and write callbacks for a specified topic to automatically apply them on any message you supply.  

To write a message add the callbacks and use the write function supplying a message and returning a buffer, e.g.:

```typescript
const readHandler = (buffer: ArrayBuffer) => {
    const decoder = new TextDecoder();
    const header = new Uint32Array(buffer.slice(0, 4));
    const length = header.at(0);
    const data = decoder.decode(buffer.slice(4, 4 + length!));

    return data;
};

const writeHandler = (data: string) => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode(data);
    const length = encoded.byteLength;

    return new Uint8Array(Array.of(
        ...new Uint8Array(new Uint32Array(Array.of(length)).buffer),
        ...encoded
    )).buffer;
};

const handler = new MessageHandler();
handler.on(0, readHandler, writeHandler);

const message = new Message(0, "test");
const buffer = handler.write(message);
```

To read a message add the callbacks and use the read function supplying a buffer and returning a generator over the retrieved messages.  
A generator is used here because it can happen that a buffer is concatenated in transfer.  

```typescript
const readHandler = (buffer: ArrayBuffer) => {
    const decoder = new TextDecoder();
    const header = new Uint32Array(buffer.slice(0, 4));
    const length = header.at(0);
    const data = decoder.decode(buffer.slice(4, 4 + length!));

    return data;
};

const writeHandler = (data: string) => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode(data);
    const length = encoded.byteLength;

    return new Uint8Array(Array.of(
        ...new Uint8Array(new Uint32Array(Array.of(length)).buffer),
        ...encoded
    )).buffer;
};

const handler = new MessageHandler();
handler.on(0, readHandler, writeHandler);

const buffer = new ArrayBuffer(); // or any other buffer you want to retrieve messages from

const messages = Array.from(handler.read(buffer));
const message = messages.at(0) as Message<string>;

// note the string type here. This can be any type you expect the message to have
```