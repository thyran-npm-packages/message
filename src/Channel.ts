import net from "net";
import Message, { MessageTopic } from "./Message";

export interface ChannelFunction {
    (socket: net.Socket, data: any): void;
}

/**
 * A channel is a standalone endpoint for features that require communication.
 * For example the plugin channel would cover all messages linked to plugins
 * 
 * This helps to capsluate ideas that might be not needed in the future and thus making it easy to remove them
 */
export default class Channel {
    private consumers: Map<MessageTopic, ChannelFunction>;

    constructor() {
        this.consumers = new Map();
    }

    receiveMessage<MessageData>(socket: net.Socket, message: Message<MessageData>) {
        const consumer = this.consumers.get(message.Topic);

        if (!consumer) {
            console.warn("No consumer found for topic on channel %d", message.Topic);
            return;
        }

        consumer(socket, message.Data);
    }

    public on(topic: MessageTopic, consumer: ChannelFunction) {
        this.consumers.set(topic, consumer);
    }
}