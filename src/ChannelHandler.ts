import net from "net";
import Channel from "./Channel";
import Message, { MessageTopic } from "./Message";

function isIterable(object: any): object is Iterable<any> {
    return object !== null && typeof object[Symbol.iterator] === 'function';
}

type OptionalIterable<T> = T | Iterable<T>;

export default class ChannelHandler {
    private readonly channels: Map<MessageTopic, Channel>;

    public constructor() {
        this.channels = new Map();
    }

    public setChannel(topic: MessageTopic, channel: Channel) {
        this.channels.set(topic, channel);
    }

    public receive<MessageData>(socket: net.Socket, data: OptionalIterable<Message<MessageData> | null>) {
        const messages = isIterable(data) ? Array.from(data) : Array.of(data);

        for (const message of messages) {
            if (message === null) {
                console.warn("Message is null");
                continue;
            }

            const topic = message.Topic;
            const channel = this.channels.get(topic);

            if (!channel) {
                console.warn("No channel for topic %d", topic);
                continue;
            }

            channel.receiveMessage(socket, message);
        }
    }
}