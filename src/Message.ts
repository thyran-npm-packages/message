export type MessageTopic = number;

export interface MessageReadHandler<MessageData> { (data: ArrayBufferLike): MessageData; }
export interface MessageWriteHandler<MessageData> { (data: MessageData): ArrayBufferLike; }

abstract class BaseMessage<DataType> {
    private readonly topic: MessageTopic;
    private readonly data: DataType;

    public constructor(topic: MessageTopic, data: DataType) {
        this.topic = topic;
        this.data = data;
    }

    public get Topic() { return this.topic; }
    public get Data() { return this.data; }
}

class RawMessage extends BaseMessage<ArrayBufferLike> {
    public toMessage<MessageData>(handler: MessageReadHandler<MessageData>) {
        const data = handler(this.Data);

        return new Message(this.Topic, data);
    }
}

export default class Message<MessageData = any> extends BaseMessage<MessageData> {
    public static readonly HEADER_LENGTH = 2;
    public static readonly HEADER_BYTE_LENGTH = Message.HEADER_LENGTH * Uint32Array.BYTES_PER_ELEMENT;

    public toBuffer(handler: MessageWriteHandler<MessageData>) {
        const body = handler(this.Data);
        const messageLength = Message.HEADER_BYTE_LENGTH + body.byteLength;
        const buffer = new ArrayBuffer(messageLength);

        const headerView = new DataView(buffer, 0, Message.HEADER_BYTE_LENGTH);
        const dataView = new Uint8Array(buffer, Message.HEADER_BYTE_LENGTH);

        headerView.setUint32(0, messageLength);
        headerView.setUint32(4, this.Topic);

        dataView.set(new Uint8Array(body));

        return buffer;
    }

    public static* fromBuffer(buffer: ArrayBufferLike) {
        let offset = 0;
        let dataOffset = Message.HEADER_BYTE_LENGTH;

        while (dataOffset <= buffer.byteLength) {
            const header = new DataView(buffer, offset, Message.HEADER_BYTE_LENGTH);

            const messageLength = header.getUint32(0);
            const topic = header.getUint32(4);

            const data = buffer.slice(dataOffset, offset += messageLength);
            
            yield new RawMessage(topic, data);

            dataOffset += messageLength;
        }
    }
}