import Message, { MessageReadHandler, MessageTopic, MessageWriteHandler } from "./Message";

export default class MessageHandler {
    private readonly readHandlers: Map<MessageTopic, MessageReadHandler<any>>;
    private readonly writeHandlers: Map<MessageTopic, MessageWriteHandler<any>>;

    constructor() {
        this.readHandlers = new Map();
        this.writeHandlers = new Map();
    }

    public* read(buffer: ArrayBufferLike) {
        const rawMessages = Message.fromBuffer(buffer);

        for (const rawMessage of rawMessages) {
            const topic = rawMessage.Topic;
            const readHandler = this.readHandlers.get(topic);

            if (!readHandler) {
                console.warn("No read handler for topic %d", topic);
                yield null;
            } else
                yield rawMessage.toMessage(readHandler);
        }
    }

    public write<MessageData>(message: Message<MessageData>) {
        const topic = message.Topic;
        const writeHandler = this.writeHandlers.get(topic);

        if (!writeHandler) {
            console.warn("No write handler for topic %d", topic);
            return null;
        }

        return message.toBuffer(writeHandler);
    }

    public on<MessageData>(topic: MessageTopic, readHandler: MessageReadHandler<MessageData>, writeHandler: MessageWriteHandler<MessageData>) {
        this.readHandlers.set(topic, readHandler);
        this.writeHandlers.set(topic, writeHandler);
    }
}