import net from "net";
import { Channel, Message } from "../src";

describe("Channel", function() {
    test.concurrent("receive message", function() {
        const testChannel = new Channel();
        testChannel.on(0, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test");
        });

        testChannel.on(1, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test2");
        });

        const socket = new net.Socket();

        const testMessage1 = new Message(0, "test");
        const testMessage2 = new Message(1, "test2");

        testChannel.receiveMessage(socket, testMessage1);
        testChannel.receiveMessage(socket, testMessage2);
    });
});