import net from "net";
import { Channel, ChannelHandler, Message } from "../src";

describe("Channel Handler", function() {
    test.concurrent("receive", function() {
        const testChannel = new Channel();
        testChannel.on(0, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test");
        });

        testChannel.on(1, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test2");
        });

        const socket = new net.Socket();
        const handler = new ChannelHandler();
        handler.setChannel(0, testChannel);
        handler.setChannel(1, testChannel);

        const testMessage1 = new Message(0, "test");
        const testMessage2 = new Message(1, "test2");

        handler.receive(socket, testMessage1);
        handler.receive(socket, testMessage2);

        handler.receive(socket, Array.of(testMessage1, testMessage2));
    });
})