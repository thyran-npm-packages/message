import net from "net";
import { Channel, ChannelHandler, Message, MessageHandler } from "../src";

describe("Combinded", function() {
    test.concurrent("test", function() {
        const readHandler = (buffer: ArrayBuffer) => {
            const decoder = new TextDecoder();
            const header = new Uint32Array(buffer.slice(0, 4));
            const length = header.at(0);
            const data = decoder.decode(buffer.slice(4, 4 + length!));
    
            return data;
        };
    
        const writeHandler = (data: string) => {
            const encoder = new TextEncoder();
            const encoded = encoder.encode(data);
            const length = encoded.byteLength;

            const buffer = new ArrayBuffer(4 + length);

            const headerView = new DataView(buffer, 0, 4);
            headerView.setUint32(0, length);

            const dataView = new Uint8Array(buffer, 4);
            dataView.set(encoded);
    
            return buffer;
        };

        const testChannel = new Channel();
        testChannel.on(0, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test");
        });

        testChannel.on(1, function(socket, data: string) {
            expect(socket).toBeDefined();

            expect(data).toBeDefined();
            expect(data).toBe("test2");
        });

        const socket = new net.Socket();

        const messageHandler = new MessageHandler();
        messageHandler.on(0, readHandler, writeHandler);
        messageHandler.on(1, readHandler, writeHandler);

        const channelHandler = new ChannelHandler();
        channelHandler.setChannel(0, testChannel);
        channelHandler.setChannel(1, testChannel);

        const testMessage1 = new Message(0, "test");
        const testMessage2 = new Message(1, "test2");

        const buffer1 = messageHandler.write(testMessage1)!;
        const buffer2 = messageHandler.write(testMessage2)!;

        const buffer = new Uint8Array(Array.of(
            ...new Uint8Array(buffer1),
            ...new Uint8Array(buffer2),
        )).buffer;

        const messages = messageHandler.read(buffer);
        
        channelHandler.receive(socket, messages);
    });
});