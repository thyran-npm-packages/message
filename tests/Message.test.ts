import { Message } from "../src";

describe("Message", function() {
    test.concurrent("to buffer", function() {
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();

        type TestData = { test: string }
        const testData: TestData = { test: "test" };

        const writeHandler = (data: TestData) => {
            const body = encoder.encode(data.test).buffer;
            const header = new Uint32Array(Array.of(body.byteLength)).buffer;

            return new Uint8Array(Array.of(
                ...new Uint8Array(header),
                ...new Uint8Array(body)
            ));
        }

        const message = new Message(0, testData);
        const buffer = message.toBuffer(writeHandler);

        expect(buffer).toBeDefined();
        expect(buffer.byteLength).toBe(Message.HEADER_BYTE_LENGTH + Uint32Array.BYTES_PER_ELEMENT + "test".length);

        const view = new Uint8Array(buffer, Message.HEADER_BYTE_LENGTH, 8);

        expect(view).toBeDefined();
        expect(view.length).toBe(8);

        const length = new Uint32Array(view, 0, 1).at(0);

        expect(length).toBeDefined();
        expect(length).toBe(4);

        const data = decoder.decode(view.subarray(4, 4 + length!));

        expect(data).toBeDefined();
        expect(data).toBe("test");
    });

    test.concurrent("from buffer", function() {
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();

        const encoded = encoder.encode("test");
        const length = encoded.byteLength;
        const messageLength = Message.HEADER_BYTE_LENGTH + 4 + encoded.byteLength;

        const buffer = new ArrayBuffer(messageLength);
        
        const headerView = new DataView(buffer, 0, Message.HEADER_BYTE_LENGTH + 4);
        headerView.setUint32(0, messageLength);
        headerView.setUint32(4, 0); // topic
        headerView.setUint32(Message.HEADER_BYTE_LENGTH, length);

        const dataView = new Uint8Array(buffer, Message.HEADER_BYTE_LENGTH + 4);
        dataView.set(encoded);

        type TestData = { test: string };

        const readHandler = (buffer: ArrayBuffer) => {
            const header = new Uint32Array(buffer.slice(0, 4));
            const length = header.at(0)!;
            const data = decoder.decode(buffer.slice(4, 4 + length));

            return { test: data } as TestData;
        };

        const messages = Message.fromBuffer(buffer);
        const rawMessage = Array.from(messages).at(0);

        expect(rawMessage).toBeDefined();

        const message = rawMessage!.toMessage(readHandler);

        expect(message).toBeDefined();
        expect(message.Topic).toBe(0);
        expect(message.Data).toHaveProperty("test");
        expect(message.Data.test).toBe("test");
    });
});