import { Message, MessageHandler } from "../src";

describe("Message Handler", function() {
    const readHandler = (buffer: ArrayBuffer) => {
        const decoder = new TextDecoder();
        const header = new Uint32Array(buffer.slice(0, 4));
        const length = header.at(0);
        const data = decoder.decode(buffer.slice(4, 4 + length!));

        return data;
    };

    const writeHandler = (data: string) => {
        const encoder = new TextEncoder();
        const encoded = encoder.encode(data);
        const length = encoded.byteLength;

        const buffer = new ArrayBuffer(4 + length);
        
        const headerView = new DataView(buffer, 0, 4);
        headerView.setUint32(0, length);

        const dataView = new Uint8Array(buffer, 4);
        dataView.set(encoded);

        return buffer;
    };

    const handler = new MessageHandler();
    handler.on(0, readHandler, writeHandler);

    test.concurrent("read", function() {
        const encoder = new TextEncoder();
        const encoded = encoder.encode("test");
        const length = encoded.byteLength;
        const messageLength = Message.HEADER_BYTE_LENGTH + 4 + encoded.byteLength;

        const buffer = new ArrayBuffer(messageLength);
        
        const headerView = new DataView(buffer, 0, Message.HEADER_BYTE_LENGTH + 4);
        headerView.setUint32(0, messageLength);
        headerView.setUint32(4, 0); // topic
        headerView.setUint32(Message.HEADER_BYTE_LENGTH, length);

        const dataView = new Uint8Array(buffer, Message.HEADER_BYTE_LENGTH + 4);
        dataView.set(encoded);

        const messages = Array.from(handler.read(buffer));
        
        expect(messages).toBeDefined();
        expect(messages).toHaveLength(1);

        const message = messages.at(0) as Message<string>;

        expect(message).toBeDefined();
        expect(message.Topic).toBe(0);
        expect(message.Data).toBe("test");
    });

    test.concurrent("write", function() {
        const message = new Message(0, "test");
        const buffer = handler.write(message);

        expect(buffer).toBeDefined();
        expect(buffer!.byteLength).toBe(Message.HEADER_BYTE_LENGTH + 4 + "test".length);
    });
});